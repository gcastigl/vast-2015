var bar_data = [
{"Day": 6, "label":"Friday", "value": 3557},
{"Day": 7, "label":"Saturday", "value": 6411},
{"Day": 8, "label":"Sunday", "value": 7570}
]

MG.data_graphic({
	title: "Visitors per day",
	description: "Visitors per day",
	data: bar_data,
	chart_type: 'bar',
	x_accessor: 'value',
	y_accessor: 'label',
	width: $( window ).width(),
	height: 300,
	right: $( window ).width()*0.175,
	left: $( window ).width()*0.1,
	animate_on_load: true,
	target: '#bar'
});

$("#bar .mg-barplot rect.mg-bar:eq(0)").css("fill", "#db4437");
$("#bar .mg-barplot rect.mg-bar:eq(1)").css("fill", "#05b378");
$("#bar .mg-barplot rect.mg-bar:eq(2)").css("fill", "#4040e8");

$('#bar text').css('font-size', '1.5rem');

function parseIsoDate(s){
	var d=new Date(s.slice(0,19).replace('T',' ')+' GMT');
	return d;
}

Date.prototype.addHours= function(h){
    this.setHours(this.getHours()+h);
    return this;
}

Date.prototype.addMinutes= function(h){
    this.setMinutes(this.getMinutes()+h);
    return this;
}

Date.prototype.hoursFormat= function(h){
	return this.getSeconds()/3600 + this.getMinutes()/60 + this.getHours();
}

var newData = [];
var days = ["Friday", "Saturday", "Sunday"];

$.getJSON( "./data/visitorsBy15mins.json", function( data ) {
	newData = data;
	// var newData = [];
	// for (var i = 0; i < data.length; i++) {
	// 	var curr = undefined;
	// 	var newCurr = undefined;
	// 	newData.push([]);
	// 	for (var j = 0; j < data[i].length; j++) {
	// 		newCurr = parseIsoDate(data[i][j]["timestamp"]).addHours(3).addHours((-24)*i);
	// 		newCurr.setMinutes(Math.floor(newCurr.getMinutes()/15)*15);
	// 		newCurr.setSeconds(0);
	// 		if(curr == undefined || curr.getHours() != newCurr.getHours() || curr.getMinutes() != newCurr.getMinutes()){
	// 			curr = newCurr;
	// 			newData[i].push({"timestamp": newCurr, "count": data[i][j]["count"]});
	// 		}
	// 	};
	// 	newCurr.addMinutes(15);
	// 	newData[i].push({"timestamp": newCurr, "count": 0});
	// };
	for (var i = 0; i < newData.length; i++) {
		for(var j = 0; j < newData[i].length; j++){
			newData[i][j].timestamp = new Date(newData[i][j].timestamp);
			newData[i][j].time = newData[i][j].timestamp.hoursFormat();
		}
	}
	var start = new Date(newData[0][0]["timestamp"]);
	start.setHours(8);
	start.setMinutes(0);
	start.setSeconds(0);
	shrink = new Date(start);
	shrink.addHours(1);
	MG.data_graphic({
		title: "Visitors by Hour",
		data: newData,
		chart_type: 'line',
		width: $( window ).width(),
		height: 300,
		right: $( window ).width()*0.175,
		left: $( window ).width()*0.1,
		bottom: 100,
		target: '#visitorsByHour',
		mouseover: function(d, i) {
			$('#visitorsByHour .mg-active-datapoint').text(d["count"] + " visitors at " + d["timestamp"].toTimeString().split(' ')[0] + " hs");
		},
		xax_count: 17,
		// regions: [{
		// 	time: [9, 11],
  // 			label: 'test' //optional
		// }],
		animate_on_load: true,
		x_accessor: 'time',
		y_accessor: 'count',
		decimals: 0,
		point_size: 4,
		markers: [{"time": start.hoursFormat(), "label": "Park opens"},{"time": shrink.hoursFormat(), "label": "Probably entrances are shrinked at this hour"}],
		brushing: true,
		x_label: 'Hour',
		y_label: 'Visitors',
		legend: ['Friday', 'Saturday', 'Sunday'],
		custom_line_color_map: [3,2,1],
		interpolate: "cardinal"
	});

});