d3.json("./data/group1data.json", function(data) {
	var markers = [{
		// Tiene que ser 13, 6
		'Check-Ins': 6,
		'label': 'fridayMarker1'
	}, {
		// Tiene que ser 15	39
		'Check-Ins': 39,
		'label': 'fridayMarker2'
	}, {
		// Tiene que ser 84, 6
		'Check-Ins': 6,
		'label': 'saturdayMarker1'
	}, {
			// Tiene que ser 37	32
		'Check-Ins': 32,
		'label': 'saturdayMarker2'
	}, {
			// Tiene que ser 69	6
		'Check-Ins': 6,
		'label': 'sundayMarker1'
	}, {
		// Tiene que ser 60	30
		'Check-Ins': 30,
		'label': 'sundayMarker2'
	}];

	lines = ['Friday', 'Saturday', 'Sunday'];

	MG.data_graphic({
		// title: "Visitors by Check-Ins",
		description: "Every day, the shape is like a Gauss Bell",
		data: data,
		chart_type: 'line',
		width: $( window ).width()*0.8,
		height: 400,
		right: $( window ).width()*0.1,
		left: 100,
		bottom: 50,
		top: 100,
		target: '#vizGroup1',
		// xax_count: 10,
		// animate_on_load: true,
		after_brushing: function(step) { 
			setTimeout(function(){
				$( "text:contains('fridayMarker1')" ).css( "fill", "red" ).text("μ - 2 𝝈");
				$( "text:contains('saturdayMarker1')" ).css( "fill", "green" ).text("μ - 2 𝝈");
				$( "text:contains('sundayMarker1')" ).css( "fill", "blue" ).text("μ - 2 𝝈");
				$( "text:contains('fridayMarker2')" ).css( "fill", "red" ).text("μ + 2 𝝈");
				$( "text:contains('saturdayMarker2')" ).css( "fill", "green" ).text("μ + 2 𝝈");
				$( "text:contains('sundayMarker2')" ).css( "fill", "blue" ).text("μ + 2 𝝈");
			},1000);
		},
		mouseover: function(d, i) {
			$('#vizGroup1 .mg-active-datapoint').text(d["Check-Ins"] + " Check-Ins by " + d["Visitors"] + " Visitors on " + lines[3-d["line_id"]]);
		},
		x_accessor: 'Check-Ins',
		y_accessor: 'Visitors',
		brushing: false,
		x_label: 'Check-Ins',
		y_label: 'Visitors',
		markers: markers,
		legend: ['Friday', 'Saturday', 'Sunday'],
		custom_line_color_map: [3,2,1],
		legend_target: "#vizGroup1Legend"
	});

	var y;
	y = parseInt($( "text:contains('fridayMarker1')" ).attr("y"));
	var f1 = $( "text:contains('fridayMarker1')" );
	f1.css( "fill", "red" ).text("μ - 2 𝝈");
	f1.attr("y", y+40);

	y = parseInt($( "text:contains('saturdayMarker1')" ).attr("y"));
	var f2 = $( "text:contains('saturdayMarker1')" );
	f2.css( "fill", "green" ).text("μ - 2 𝝈");
	f2.attr("y", y+20);

	// $( "text:contains('sundayMarker1')" ).css( "fill", "blue" ).text("μ - 2 𝝈");
	y = parseInt($( "text:contains('sundayMarker1')" ).attr("y"));
	var f3 = $( "text:contains('sundayMarker1')" );
	f3.css( "fill", "blue" ).text("μ - 2 𝝈");
	f3.attr("y", y);

	// $( "text:contains('fridayMarker2')" ).css( "fill", "red" ).text("μ + 2 𝝈");
	y = parseInt($( "text:contains('fridayMarker2')" ).attr("y"));
	var f4 = $( "text:contains('fridayMarker2')" );
	f4.css( "fill", "red" ).text("μ + 2 𝝈");
	f4.attr("y", y+40);

	// $( "text:contains('saturdayMarker2')" ).css( "fill", "green" ).text("μ + 2 𝝈");
	y = parseInt($( "text:contains('saturdayMarker2')" ).attr("y"));
	var f5 = $( "text:contains('saturdayMarker2')" );
	f5.css( "fill", "green" ).text("μ + 2 𝝈");
	f5.attr("y", y+20);

	// $( "text:contains('sundayMarker2')" ).css( "fill", "blue" ).text("μ + 2 𝝈");
	y = parseInt($( "text:contains('sundayMarker2')" ).attr("y"));
	var f1 = $( "text:contains('sundayMarker2')" );
	f1.css( "fill", "blue" ).text("μ + 2 𝝈");
	f1.attr("y", y);
});

d3.json("./data/group_4_5_Data.json", function(data) {
	var days = ["Friday", "Saturday", "Sunday"];
	MG.data_graphic({
		// title: "Amount of Check-Ins by Attraction",
		description: "Including entrances and park check-ins",
		data: data,
		x_label: '<- Ride ID ->',
		y_label: 'Favorism Percentage',
		chart_type: 'point',
		width: $( window ).width()*0.85,
		height: 400,
		left: 100,
		bottom: 100,
		top: 100,
		yax_count: 5,
		right: $( window ).width()*0.07,
		mouseover: function(d, i) {
			$('#vizGroup4_5 .mg-active-datapoint').text(
				data[i]["Name"] + " - " + days[data[i]["Day"]-6] + " (" + (data[i]["Check-Ins"] * 100).toFixed(2) + "%)"
			);
		},
		target: '#vizGroup4_5',
		xax_count: 0,
		// x_axis: false,
		animate_on_load: true,
		x_accessor: 'Ride ID',
		y_accessor: 'Check-Ins',
        color_accessor: 'Day',
		color_range: ["red","green","blue"],
        baselines: [
			{value: 0.05, label: "Unpopular baseline"},
			{value: 0.10, label: "Popular baseline"}
		],
        markers: [
			{'Ride ID': 32, 'label': 'Check out this attraction, it has much less views on Sunday than the rest of the days!'}
			// {'Ride ID': 81, 'label': 'This attraction too!'}
		],
        color_type:'category'
	});

	$('div#vizGroup4_5 .mg-markers text').css('font-size', '1rem');
	$('div#vizGroup4_5 .mg-baselines text').css('font-size', '1rem');
});

// GRAFICO DE BARRAS 1
var bar_data = [
	{"value": 76, "name": "Friday"},
	{"value": 143, "name": "Saturday"},
	{"value": 192, "name": "Sunday"}
];

MG.data_graphic({
	title: "Sizes of group 1 per day",
	description: "Size group 1",
	data: bar_data,
	chart_type: 'bar',
	x_accessor: 'value',
	y_accessor: 'name',
	width: 900,
	height: 200,
	right: 10,
	animate_on_load: true,
	target: '#bar_1'
});

$("#bar_1 .mg-barplot rect.mg-bar:eq(0)").css("fill", "#db4437");
$("#bar_1 .mg-barplot rect.mg-bar:eq(1)").css("fill", "#05b378");
$("#bar_1 .mg-barplot rect.mg-bar:eq(2)").css("fill", "#4040e8");

// GRAFICO DE BARRAS 2
var bar_data = [
	{"value": 3394, "name": "Friday"},
	{"value": 6121, "name": "Saturday"},
	{"value": 7230, "name": "Sunday"}
];

MG.data_graphic({
	title: "Sizes of group 2 per day",
	description: "Size group 2",
	data: bar_data,
	chart_type: 'bar',
	x_accessor: 'value',
	y_accessor: 'name',
	width: 900,
	height: 200,
	right: 10,
	animate_on_load: true,
	target: '#bar_2'
});

$("#bar_2 .mg-barplot rect.mg-bar:eq(0)").css("fill", "#db4437");
$("#bar_2 .mg-barplot rect.mg-bar:eq(1)").css("fill", "#05b378");
$("#bar_2 .mg-barplot rect.mg-bar:eq(2)").css("fill", "#4040e8");

// GRAFICO DE BARRAS 3
var bar_data = [
	{"value": 87, "name": "Friday"},
	{"value": 147, "name": "Saturday"},
	{"value": 148, "name": "Sunday"}
];

MG.data_graphic({
	title: "Sizes of group 3 per day",
	description: "Size group 3",
	data: bar_data,
	chart_type: 'bar',
	x_accessor: 'value',
	y_accessor: 'name',
	width: 900,
	height: 200,
	right: 10,
	animate_on_load: true,
	target: '#bar_3'
});

$("#bar_3 .mg-barplot rect.mg-bar:eq(0)").css("fill", "#db4437");
$("#bar_3 .mg-barplot rect.mg-bar:eq(1)").css("fill", "#05b378");
$("#bar_3 .mg-barplot rect.mg-bar:eq(2)").css("fill", "#4040e8");


/*
MG.data_graphic({
	title: "Missing Data",
	description: "This is an example of a graphic whose data is currently missing. We've also set the error option, which appends an error icon to the title and logs an error to the browser's console.",
	error: 'This data is blocked by Lorem Ipsum. Get your **** together, Ipsum.',
	chart_type: 'missing-data',
	missing_text: 'This is an example of a missing chart',
	target: '#missingPlot1',
	width:  $( window ).width()*0.8,
	height: 200
});

MG.data_graphic({
	title: "Missing Data",
	description: "This is an example of a graphic whose data is currently missing. We've also set the error option, which appends an error icon to the title and logs an error to the browser's console.",
	error: 'This data is blocked by Lorem Ipsum. Get your **** together, Ipsum.',
	chart_type: 'missing-data',
	missing_text: 'This is an example of a missing chart',
	target: '#missingPlot2',
	width:  $( window ).width()*0.8,
	height: 200
});*/

// Grafico para grupos 6 y 7

d3.json("./data/group_6_7.json", function(data) {

	lines = ['Friday', 'Saturday', 'Sunday'];
	
    MG.data_graphic({
        description: "Amount of visitors entering the park per hour.",
        data: data,
        width: $(window).width()*0.85,
        height: 300,
        right: $(window).width()*0.07,
		x_accessor: 'hour',
		y_accessor: 'visitors',
        target: '#group_6_7_plot',
        legend: ['Friday', 'Saturday', 'Sunday'],
		custom_line_color_map: [3,2,1],
        legend_target: '#group_6_7_legend'
    });
});

// Grafico para grupos 8 y 9

d3.json("./data/group_8_9.json", function(data) {

	lines = ['Friday', 'Saturday', 'Sunday'];
	
    MG.data_graphic({
        description: "Amount of visitors leaving the park per hour.",
        data: data,
        width: $(window).width()*0.85,
        height: 300,
        right: $(window).width()*0.07,
		x_accessor: 'hour',
		y_accessor: 'visitors',
        target: '#group_8_9_plot',
		custom_line_color_map: [3,2,1],
        legend: ['Friday', 'Saturday', 'Sunday'],
        legend_target: '#group_8_9_legend'
    });
});