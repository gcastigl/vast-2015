-- Visitante, hora de entradas (primera)

select distinct visitorid, min(hour(timestamp)) as 'First Access'
from checkins
where day(timestamp) = 7
group by visitorid
having hour(min(time(timestamp))) <= 10 	-- personas que entran temprano
-- having hour(min(time(timestamp))) > 13	-- personas que entran tarde
limit 999999;

-- Cantidad de checkins de los visitantes por hora:

select Entrada, count(*) as '#Check Ins'
from (
	select visitorid, hour(min(timestamp)) as 'Entrada'
	from checkins
	where day(timestamp) = 8
	group by visitorid
) as lala
group by Entrada;

