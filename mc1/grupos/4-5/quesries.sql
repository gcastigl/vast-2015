-- Atracciones favoritas (> 400) y no favoritas (<300)

	select atr.ride_id
	from cant_visitas cv1
		inner join (
			select visitorid, MAX(cant) as "MAX_CANT"
			from cant_visitas
			where day = 6
			group by visitorid
		) as cv2
	ON cv1.visitorid = cv2.visitorid and cv1.cant = MAX_CANT
		and day = 6
	JOIN atracciones atr ON atr.x = cv1.x AND atr.y = cv1.y
	group by cv1.x, cv1.y
	HAVING COUNT(*) < 300;


-- Cantidad de visitantes que favoritearon segun identificador de atraccion 

	select COUNT(distinct cv1.visitorId) 
	from cant_visitas cv1
		inner join (
			select visitorid, MAX(cant) as "MAX_CANT"
			from cant_visitas
			where day = 6
			group by visitorid
		) as cv2
		ON cv1.visitorid = cv2.visitorid and cv1.cant = MAX_CANT
		JOIN atracciones atr ON atr.x = cv1.x AND atr.y = cv1.y
	where day = 6
		and atr.ride_id IN(1,2,3,4,5,6,7,8,32,81) -- populares
		-- and atr.ride_id IN(9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,62,63,64) -- No populares
	;

-- Viernes | F = 3319, NF = 1043
-- Sabado | F = 5509, NF = 2341
-- Domingo | F = 6446, NF = 3075

