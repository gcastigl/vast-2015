select day, Salida as "hour", count(*) as 'visitors'
from (
	select visitorid, hour(max(timestamp)) as 'Salida', day(timestamp) as "day"
	from movements
	group by day(timestamp), visitorid
) as lala
group by Salida, day
order by day asc, hour(day) asc;