import oursql
import json


with open('credentials.json') as data_file:
    cred = json.load(data_file)

db_connection = oursql.connect(host=cred["host"],user=cred["user"],passwd=cred["passwd"],db=cred["db"])

cur=db_connection.cursor()

cur.execute("""
select hour(timestamp), minute(timestamp), day(timestamp), a1.name, checkins.visitorid
from checkins INNER JOIN atracciones a1 on checkins.x = a1.x and checkins.y = a1.y
where checkins.visitorid not in (
  select cv.visitorid
  from cant_visitas cv INNER JOIN atracciones a2 on cv.x = a2.x and cv.y = a2.y
  where a2.ride_id > 0 and a2.ride_id <> 32 and a2.ride_id <> 63 and a2.ride_id <> 62 and cv.cant > 0
)
""")
friday = []
for row in cur.fetchall():
    print(row)
    # friday.append({"time": float(row[0]) + float((row[1])//15)/4.0, "day": row[2], "ride_name": row[3], "visitor_id": row[4]})

# print(friday)

# cur=db_connection.cursor()
# cur.execute("""
# select distinct visitorid, minute(min(timestamp)), min(hour(timestamp)) as 'First Access'
# from checkins
# where day(timestamp) = 7
# group by visitorid
# having min(hour(timestamp)) <= 24
# """)
# sat = []
# for row in cur.fetchall():
#     sat.append(float(row[2]) + float((row[1])//15)/4.0)
#
# cur=db_connection.cursor()
# cur.execute("""
# select distinct visitorid, minute(min(timestamp)), min(hour(timestamp)) as 'First Access'
# from checkins
# where day(timestamp) = 8
# group by visitorid
# having min(hour(timestamp)) <= 24
# """)
# sun = []
# for row in cur.fetchall():
#     sun.append(float(row[2]) + float((row[1])//15)/4.0)
#
#
# import numpy as np
# import seaborn as sns
# import matplotlib.pyplot as plt
#
# sns.set(style="dark", palette="muted")
#
# b, g, r, p = sns.color_palette("muted", 4)
#
# rs = np.random.RandomState(10)
# d = rs.normal(size=100)
#
# graphic = sns.barplot(friday, color=r)
# graphic.set(xlim=(min(friday), max(friday)))
# sns.barplot(sat, color=g)
# sns.barplot(sun, color=b)
#
# plt.tight_layout()
# sns.despine(left=True, bottom=True)
# sns.plt.show()