import oursql
import json

with open('credentials.json') as data_file:
    cred = json.load(data_file)

db_connection = oursql.connect(host=cred["host"],user=cred["user"],passwd=cred["passwd"],db=cred["db"])

cur=db_connection.cursor()

group_4_5_Data = []
maximums = []
for i in range(6, 9):
    day = str(i)
    cur.execute("""
    select COUNT(distinct(visitorid))
    from checkins
    where day(checkins.timestamp) = """ + day + """
    """)
    for row in cur.fetchall():
        maximums.append(row[0])

print(maximums)

# group_4_5_Data = []
for i in range(6, 9):
    day = str(i)
    cur.execute("""
    select atr.ride_id, timesFavorited, atr.name
    from atracciones atr
        inner join(
            select cv1.x, cv1.y, COUNT(*) as 'timesFavorited'
            from cant_visitas cv1
                inner join (
                    select visitorid, MAX(cant) as "MAX_CANT"
                    from cant_visitas
                    where day = """ + day + """
                    group by visitorid
                ) as cv2
            ON cv1.visitorid = cv2.visitorid and cv1.cant = MAX_CANT
            where day = """ + day + """
            group by cv1.x, cv1.y
        ) as intern
    ON intern.x = atr.x and intern.y = atr.y
    where atr.ride_id >= 0
    """)
    # group_4_5_DataDay = []
    for row in cur.fetchall():
        if(row[0] > 100):
            group_4_5_Data.append({"Day": i, "Check-Ins": row[1]/maximums[i-6], "Ride ID": row[0]-800, "Name": row[2]})
        else:
            group_4_5_Data.append({"Day": i, "Check-Ins": row[1]/maximums[i-6], "Ride ID": row[0], "Name": row[2]})
    # group_4_5_Data.append(group_4_5_DataDay)


print(group_4_5_Data)

sum = 0
for data in group_4_5_Data:
    sum += data["Check-Ins"]

print(sum)

with open('../informe/data/group_4_5_Data.json', 'w') as outfile:
    json.dump(group_4_5_Data, outfile)
