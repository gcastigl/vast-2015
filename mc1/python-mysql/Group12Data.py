import oursql
import json

group12Data = []

with open('credentials.json') as data_file:
    cred = json.load(data_file)

db_connection = oursql.connect(host=cred["host"],user=cred["user"],passwd=cred["passwd"],db=cred["db"])

cur=db_connection.cursor()

everyDay = 0

cur.execute("""
select count(*)
from(
    select visitorid
    from checkins
    group by visitorid
    having count(distinct DAY(timestamp)) = 3
) as lala
""")

for row in cur.fetchall():
    everyDay = row[0]
    group12Data.append({"id": "Visited Every Day", "value": row[0]})


cur.execute("""
    select count(distinct visitorid)
    from cant_visitas
""")

for row in cur.fetchall():
    group12Data.append({"id": "Others", "value": row[0] - everyDay})

with open('../informe/data/group12data.json', 'w') as outfile:
    json.dump(group12Data, outfile)