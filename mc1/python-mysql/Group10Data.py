import oursql
import json

with open('credentials.json') as data_file:
    cred = json.load(data_file)

db_connection = oursql.connect(host=cred["host"],user=cred["user"],passwd=cred["passwd"],db=cred["db"])

cur=db_connection.cursor()

cur.execute("""
select hour(timestamp), minute(timestamp), day(timestamp), a1.name, checkins.visitorid
from checkins INNER JOIN atracciones a1 on checkins.x = a1.x and checkins.y = a1.y
where checkins.visitorid not in (
  select cv.visitorid
  from cant_visitas cv INNER JOIN atracciones a2 on cv.x = a2.x and cv.y = a2.y
  where a2.ride_id > 0 and a2.ride_id <> 32 and a2.ride_id <> 63 and a2.ride_id <> 62 and cv.cant > 0
)
""")
group10 = []
for row in cur.fetchall():
    group10.append({"time": float(row[0]) + float((row[1])//15)/4.0, "day": row[2], "ride_name": row[3], "visitor_id": row[4]})

print(group10)

with open('../informe/data/group10data.json', 'w') as outfile:
    json.dump(group10, outfile)

