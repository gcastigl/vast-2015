import oursql
import json
import datetime

with open('credentials.json') as data_file:
    cred = json.load(data_file)

db_connection = oursql.connect(host=cred["host"],user=cred["user"],passwd=cred["passwd"],db=cred["db"])

cur=db_connection.cursor()

dthandler = lambda obj: (
    obj.isoformat()
    if isinstance(obj, datetime.datetime)
    or isinstance(obj, datetime.date)
    else None)

visitorsByHour = []

for i in range(6, 9):
    day = str(i)
    people = []
    cur.execute("""
    select inc, ts
    from(select 1 as 'inc', min(timestamp) as 'ts'
	from movements
	where day(timestamp) = """ + day + """
	group by visitorid
	UNION ALL
	select -1 as 'inc', max(timestamp) as 'ts'
	from movements
	where day(timestamp) = """ + day + """
	group by visitorid
	) as lala
	order by ts
    """)
    count = 0
    for row in cur.fetchall():
        count += row[0]
        people.append({"count": count, "timestamp": json.dumps(row[1], default=dthandler)[1:-1], "day": day})
    visitorsByHour.append(people)

with open('../informe/data/visitorsByHour.json', 'w') as outfile:
    json.dump(visitorsByHour, outfile)
