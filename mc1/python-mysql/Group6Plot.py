from scipy.stats import norm
import matplotlib.mlab as mlab
import matplotlib.lines as mlines
import matplotlib.pyplot as plt
import numpy as np

# read data from a text file. One number per line
with open("/home/apierri/ITBA/2015-1/VISUALIZACION-DE-INFORMACION-72.74/vast-2015/mc1/grupos/6/FridayFirstAccess.csv") as f:
    hour = []
    amount = []
    f.readline()
    for line in f.readlines():
        actual_line = line.rstrip('\n').rsplit(',')
        hour.append(float(actual_line[0]))
        amount.append(int(actual_line[1]))
amount[:] = [x / sum(amount) for x in amount]
plt.plot(hour, amount, 'r-')
plt.scatter(hour, amount, color='red')
topAmount = max(amount)


with open("/home/apierri/ITBA/2015-1/VISUALIZACION-DE-INFORMACION-72.74/vast-2015/mc1/grupos/6/SaturdayFirstAccess.csv") as f:
    hour = []
    amount = []
    f.readline()
    for line in f.readlines():
        actual_line = line.rstrip('\n').rsplit(',')
        hour.append(float(actual_line[0]))
        amount.append(int(actual_line[1]))
amount[:] = [x / sum(amount) for x in amount]
plt.plot(hour, amount, 'g-')
plt.scatter(hour, amount, color='green')
topAmount = max(max(amount), topAmount)


with open("/home/apierri/ITBA/2015-1/VISUALIZACION-DE-INFORMACION-72.74/vast-2015/mc1/grupos/6/SundayFirstAccess.csv") as f:
    hour = []
    amount = []
    f.readline()
    for line in f.readlines():
        actual_line = line.rstrip('\n').rsplit(',')
        hour.append(float(actual_line[0]))
        amount.append(int(actual_line[1]))
amount[:] = [x / sum(amount) for x in amount]
plt.plot(hour, amount, 'b-')
plt.scatter(hour, amount, color='blue')
topAmount = max(max(amount), topAmount)

#plot
fridayLeg = mlines.Line2D([], [], color='red', marker='o',markersize=5, label='Friday')
saturdayLeg = mlines.Line2D([], [], color='green', marker='o',markersize=5, label='Saturday')
sundayLeg = mlines.Line2D([], [], color='blue', marker='o',markersize=5, label='Sunday')
plt.legend(handles=[fridayLeg, saturdayLeg, sundayLeg])
plt.xlabel('Check-In Hour')
plt.ylabel('Percentage of First-Access of visitors for the day')
plt.xlim((min(hour),max(hour)))
plt.ylim((0,max(amount)*1.1))
plt.title('Entrance of People')
plt.grid(True)

plt.show()