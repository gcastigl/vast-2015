import oursql
import json

with open('credentials.json') as data_file:
    cred = json.load(data_file)

db_connection = oursql.connect(host=cred["host"],user=cred["user"],passwd=cred["passwd"],db=cred["db"])

cur=db_connection.cursor()
group1Data = []
for i in range(6, 9):
    day = str(i)
    cur.execute("""
        select count(*) as 'CANT_PERSONAS', CHECK_INS
        from
            (
                select visitorId, count(*) as "CHECK_INS"
                from checkins
                where
                    not ((x = 63 AND y = 99) or (x = 0 AND y = 67) or (x = 99 AND y = 77))
                    and day(timestamp) = """ + day + """
                group by visitorId
            ) as vp
        group by CHECK_INS
        UNION ALL
        (
            select count(distinct visitorid), 0
            from checkins c1
            where day(timestamp) = """ + day + """ and NOT EXISTS(
                select *
                from checkins c2
                where c1.visitorid = c2.visitorid and
                    not ((x = 63 AND y = 99) or (x = 0 AND y = 67) or (x = 99 AND y = 77))
                    and day(c2.timestamp) = """ + day + """
            )
        );
    """)
    group1DataDay = []
    print(i)
    ciCount = 1
    for row in cur.fetchall():
        while(ciCount < row[1]):
            group1DataDay.append({"Day": i, "Check-Ins": ciCount, "Visitors": 0})
            ciCount += 1
        group1DataDay.append({"Day": i, "Check-Ins": row[1], "Visitors": row[0]})
        ciCount += 1
    group1Data.append(group1DataDay)


print(group1Data)
#
with open('../informe/data/group1data.json', 'w') as outfile:
    json.dump(group1Data, outfile)
