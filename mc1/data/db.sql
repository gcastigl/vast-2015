-- Tabla con todos los datos (movimeintos y checkins)

	CREATE TABLE `movements` (
	  `id` INT NOT NULL AUTO_INCREMENT,
	  `timestamp` DATETIME NOT NULL,
	  `visitorid` VARCHAR(20) NOT NULL,
	  `type` VARCHAR(20) NOT NULL,
	  `x` INT NOT NULL,
	  `y` INT NOT NULL,
	  PRIMARY KEY (`id`));

-- Importacion de datos

	LOAD DATA INFILE '/tmp/park-movement-Fri.csv'
	INTO TABLE movements
	FIELDS TERMINATED BY ','
	(timestamp, visitorid, type, x, y);

	LOAD DATA INFILE '/tmp/park-movement-Sat.csv'
	INTO TABLE movements
	FIELDS TERMINATED BY ','
	(timestamp, visitorid, type, x, y);

	LOAD DATA INFILE '/tmp/park-movement-Sun.csv'
	INTO TABLE movements
	FIELDS TERMINATED BY ','
	(timestamp, visitorid, type, x, y);


-- Tabla con solo los check ins

	CREATE TABLE `checkins` (
	  `id` int(11) NOT NULL AUTO_INCREMENT,
	  `timestamp` datetime NOT NULL,
	  `visitorid` varchar(20) NOT NULL,
	  `type` varchar(20) NOT NULL,
	  `x` int(11) NOT NULL,
	  `y` int(11) NOT NULL,
	  PRIMARY KEY (`id`),
	  KEY `inx_visitorid` (`visitorid`),
	  KEY `indx_type` (`type`),
	  KEY `indx_timestamp` (`timestamp`)
	);

	insert into checkins(timestamp, visitorid, type, x, y)
		select timestamp, visitorid, 'check-in', x, y
		from movements
		where type = 'check-in';

-- Tabla con la cantida de visitas a cada juegos

	CREATE TABLE `cant_visitas` (
	  `id` INT NOT NULL AUTO_INCREMENT,
	  `day` INT NOT NULL,
	  `visitorid` VARCHAR(20) NOT NULL,
	  `x` VARCHAR(45) NOT NULL,
	  `y` INT NOT NULL,
	  `cant` INT NOT NULL,
	  PRIMARY KEY (`id`));

	insert into cant_visitas(day, visitorid, x, y, cant)
		select day(timestamp), visitorid, x, y, count(*)
		from checkins cf2 
		group by day(timestamp), visitorid, x, y;

-- Localizacion de las otrascciones visitadas

	CREATE TABLE `atracciones` (
	  `id` INT NOT NULL AUTO_INCREMENT,
	  `x` INT NOT NULL,
	  `y` INT NOT NULL,
	  `ride_id` INT NOT NULL,
	  `name` VARCHAR(128) NOT NULL,
	  `category` VARCHAR(128) NOT NULL,
	  PRIMARY KEY (`id`));

	ALTER TABLE `atracciones` 
		ADD INDEX `index_x` (`x` ASC),
		ADD INDEX `index_y` (`y` ASC);

	LOAD DATA INFILE '/tmp/attractions.csv'
	INTO TABLE atracciones
	FIELDS TERMINATED BY ','
	(x, y, ride_id, name, category);


-- Consultas para cada grupo

-- 6 = Viernes
-- Entradas:
	-- 63, 99
	-- 0, 67
	-- 99, 77

-- Grupo 1, 2, 3

	select count(*) as "CANT_PERSONAS", CHECK_INS
	from
		(
			select visitorId, count(*) as "CHECK_INS"
			from checkins
			where 
				not ((x = 63 AND y = 99) or (x = 0 AND y = 67) or (x = 99 AND y = 77))
				and day(timestamp) = '6'
			group by visitorId
		) as vp
	group by CHECK_INS
	UNION ALL
	(
		select count(distinct visitorid), 0
		from checkins c1
		where day(timestamp) = '6' and NOT EXISTS(
			select *
			from checkins c2
			where c1.visitorid = c2.visitorid and
				not ((x = 63 AND y = 99) or (x = 0 AND y = 67) or (x = 99 AND y = 77))
				and day(c2.timestamp) = '6'
		)
	);

	-- Para el inciso B
	
	select distinct CONCAT("(", x, ",", y, ")")
	from(
		select visitorid, SUM(cant)
		from cant_visitas
		where day = 6 and not ((x = 63 AND y = 99) or (x = 0 AND y = 67) or (x = 99 AND y = 77))
		group by visitorid
		having 6 < SUM(cant) and SUM(cant) < 40
		-- having SUM(cant) <= 6
		-- having 40 <= SUM(cant)
	) pepe
	JOIN checkins c ON c.visitorid =  pepe.visitorid and day(c.timestamp) = 6
	where not ((x = 63 AND y = 99) or (x = 0 AND y = 67) or (x = 99 AND y = 77));




-- Grupo 4, 5

	-- Por juego, la cantidad de personas que lo "favoritearon"

	select * 
	from cant_visitas cv1
		inner join (
			select visitorid, MAX(cant) as "MAX_CANT"
			from cant_visitas
			where day = 6
			group by visitorid
		) as cv2
	ON cv1.visitorid = cv2.visitorid and cv1.cant = MAX_CANT
	where day = 6;

	-- EL divisor entre popular y no polular es COUNT(*) = 400

	select cv1.x, cv1.y, COUNT(*)
	from cant_visitas cv1
		inner join (
			select visitorid, MAX(cant) as "MAX_CANT"
			from cant_visitas
			where day = 6
			group by visitorid
		) as cv2
	ON cv1.visitorid = cv2.visitorid and cv1.cant = MAX_CANT
		and day = 6
	group by cv1.x, cv1.y;

-- inciso b, para ver que atracciones son las populares
-- Son todas de Thrill rides category
-- Y la atraccion Creighton Pavilion que es la 32 "que el domingo pasa algo"
select * 
from atracciones atr
	inner join(	
		select cv1.x, cv1.y, COUNT(*)
		from cant_visitas cv1
			inner join (
				select visitorid, MAX(cant) as "MAX_CANT"
				from cant_visitas
				where day = 6
				group by visitorid
			) as cv2
		ON cv1.visitorid = cv2.visitorid and cv1.cant = MAX_CANT
			and day = 6
		group by cv1.x, cv1.y
	) as intern
ON intern.x = atr.x and intern.y = atr.y;

-- Atracciones populares

select inter.x, inter.y, ride_id, name, category, count
from atracciones atr
	inner join(	
		select cv1.x, cv1.y, COUNT(*) AS count
		from cant_visitas cv1
			inner join (
				select visitorid, MAX(cant) as "MAX_CANT"
				from cant_visitas
				where day = 6
				group by visitorid
			) as cv2
		ON cv1.visitorid = cv2.visitorid and cv1.cant = MAX_CANT
			and day = 6
		group by cv1.x, cv1.y
	) as inter
ON inter.x = atr.x and inter.y = atr.y
where count > 400;

-- Atracciones no populares

select inter.x, inter.y, ride_id, name, category, count
from atracciones atr
	inner join(	
		select cv1.x, cv1.y, COUNT(*) AS count
		from cant_visitas cv1
			inner join (
				select visitorid, MAX(cant) as "MAX_CANT"
				from cant_visitas
				where day = 6
				group by visitorid
			) as cv2
		ON cv1.visitorid = cv2.visitorid and cv1.cant = MAX_CANT
			and day = 6
		group by cv1.x, cv1.y
	) as inter
ON inter.x = atr.x and inter.y = atr.y
where count < 400;





-- Salida de los visitantes agupadas por hora:

	select salida, count(*)
	from (
		select visitorid, hour(MAX(timestamp)) as 'salida'
		from movements
		group by visitorid
	) as salidas
	group by salida;

-- Personas que fueron al parque los 3 dias seguidos:

	select visitorid
	from movements
	where type = 'check-in'
	group by visitorid
	having count(distinct day(timestamp)) = 3;

