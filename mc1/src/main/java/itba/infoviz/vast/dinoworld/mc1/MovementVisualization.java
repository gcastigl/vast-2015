package itba.infoviz.vast.dinoworld.mc1;

import java.io.File;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import org.joda.time.DateTime;
import org.joda.time.Hours;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.KeyListener;
import org.newdawn.slick.SlickException;

public class MovementVisualization extends BasicGame {

	public static void main(String[] args) throws SlickException {
		AppGameContainer app = new AppGameContainer(new MovementVisualization());
		app.setShowFPS(false);
		app.setDisplayMode(700, 700, false);
		app.start();
	}

	private static final Color[] COLORS = { Color.orange, Color.pink, Color.cyan, Color.darkGray, Color.gray, Color.green, Color.lightGray, Color.magenta,
			Color.black, Color.blue };

	private Image _parkMap;
	private ParkMovements _movements;
	private Map<String, ParkMovementLine> _visitors = new HashMap<>();
	private DateTime _currentTime = new DateTime(2014, 6, 06, 8, 0);
	private int _timeScale = 1;
	private boolean _endOfSim;

	public MovementVisualization() {
		super("Dinofun World!");
	}

	@Override
	public void init(GameContainer container) throws SlickException {
		_parkMap = new Image("./data/Park Map-small.jpg");
		_movements = new ParkMovements(new File("./data/MC1 2015 Data"), new String[] { "park-movement-Fri.csv", "park-movement-Sat.csv",
				"park-movement-Sun.csv" });
		container.getInput().addKeyListener(new KeyListenerImpl(container));
		_endOfSim = false;
	}

	@Override
	public void update(GameContainer container, int tpf) throws SlickException {
		if (!_endOfSim) {
			if (!container.isPaused()) {
				_currentTime = _currentTime.plusMillis(tpf * _timeScale);
				_movements.readUnitDateTime(_currentTime, _visitors);
			}
			// Remove visitors that have already left the park
			for (ParkMovementLine line : new LinkedList<>(_visitors.values())) {
				if (isAtExit(line) && Hours.hoursBetween(line.timestamp(), _currentTime).getHours() > 1) {
					_visitors.remove(line.visitorId());
				}
			}
			if (_movements.reachedEndOfDay() && _currentTime.getDayOfMonth() < 8) {
				_movements.useNextFile();
				_currentTime = _currentTime.plusDays(1).withTime(8, 0, 0, 0);
				_visitors.clear();
			}
		}
	}

	private boolean isAtExit(ParkMovementLine line) {
		if (line.x() == 63 && line.y() == 99) {
			return true;
		}
		// TODO: add other exits!!
		return false;
	}

	private static final Color backgroundFilter = new Color(0.3f, 0.3f, 0.3f);

	public void render(GameContainer container, Graphics g) throws SlickException {
		_parkMap.draw(0, 0, 0.33f, backgroundFilter);
		final float X_POS_TO_SCEEN_OFFSET = 6.6f;
		final float Y_POS_TO_SCEEN_OFFSET = 7.35f;
		for (ParkMovementLine line : _visitors.values()) {
			if (!line.type().equals("check-in")) {
				String visitorId = line.visitorId();
				g.setColor(COLORS[Integer.valueOf(visitorId.charAt(visitorId.length() - 1)) - '0']);
				float x = line.x() * X_POS_TO_SCEEN_OFFSET;
				float y = line.y() * Y_POS_TO_SCEEN_OFFSET;
				g.fillOval(x, 20 + (container.getHeight() - y) * 0.9f, 5, 5);
			}
		}
		String timeString = String.format("%s - %02d:%02d:%02d", _currentTime.dayOfWeek().getAsShortText(), _currentTime.getHourOfDay(), _currentTime
				.minuteOfHour().get(), _currentTime.getSecondOfMinute());
		g.setColor(Color.red);
		g.drawString(timeString, 10, 660);
		g.drawString("Visitors: " + _visitors.size(), 10, 675);
	}

	private final class KeyListenerImpl implements KeyListener {

		private GameContainer _container;

		public KeyListenerImpl(GameContainer container) {
			_container = container;
		}

		@Override
		public void setInput(Input input) {
		}

		@Override
		public boolean isAcceptingInput() {
			return true;
		}

		@Override
		public void inputEnded() {
		}

		@Override
		public void inputStarted() {
		}

		@Override
		public void keyPressed(int key, char c) {
		}

		@Override
		public void keyReleased(int key, char c) {
			if (c == 'p') {
				_container.setPaused(!_container.isPaused());
			} else if (c == '1') {
				_timeScale = Math.min(0, _timeScale - 1);
			} else if (c == '2') {
				_timeScale++;
			} else if (c == '3') {
				_timeScale += 20;
			} else if (c == '0') {
				_timeScale = 1;
			} else if (key == Input.KEY_ESCAPE) {
				_container.exit();
			}
		}

	}
}
