package itba.infoviz.vast.dinoworld.mc1;

import itba.infoviz.vast.dinoworld.util.PeekableScanner;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Map;
import java.util.Scanner;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class ParkMovements {

	private static final DateTimeFormatter formatter = DateTimeFormat.forPattern("y-M-d H:m:s");

	private final File _directory;
	private final String[] _files;
	private int _fileIndex;
	private PeekableScanner _source;
	private int _lineIndex = 0;

	public ParkMovements(File directory, String[] files) {
		_directory = directory;
		_files = files;
		useNextFile();
	}

	public void useNextFile() {
		try {
			System.out.println("using file: " + _files[_fileIndex]);
			File file = new File(_directory + "/" + _files[_fileIndex]);
			_source = new PeekableScanner(new Scanner(file));
			_source.nextLine();
			_lineIndex = 0;
			_fileIndex++;
		} catch (FileNotFoundException e) {
			throw new IllegalStateException(e);
		}
	}

	public void readUnitDateTime(DateTime dateTime, Map<String, ParkMovementLine> lines) {
		boolean lineDateTimeIsBefore = true;
		do {
			String line = _source.peekLine();
			if (line != null) {
				String[] columns = line.split(",");
				DateTime timestamp = formatter.parseDateTime(columns[0]);
				lineDateTimeIsBefore = timestamp.isBefore(dateTime); 
				if (lineDateTimeIsBefore) {
					ParkMovementLine movement = new ParkMovementLine()
						.setIndex(_lineIndex++)
						.setTimestamp(timestamp)
						.setVisitorId(columns[1])
						.setType(columns[2])
						.setX(Integer.valueOf(columns[3]))
						.setY(Integer.valueOf(columns[4]))
					;
					lines.put(movement.visitorId(), movement);
					_source.consume();
				}
			} else {
				lineDateTimeIsBefore = false;
			}
		} while(lineDateTimeIsBefore);
	}
	
	public boolean reachedEndOfDay() {
		return !_source.hasNextLine();
	}
}
