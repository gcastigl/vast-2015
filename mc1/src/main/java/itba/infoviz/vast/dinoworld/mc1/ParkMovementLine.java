package itba.infoviz.vast.dinoworld.mc1;

import org.joda.time.DateTime;

public class ParkMovementLine {

	private int _index;
	private DateTime _timestamp;
	private float _x, _y;
	private String _type;
	private String _visitorId;

	public int index() {
		return _index;
	}

	public DateTime timestamp() {
		return _timestamp;
	}

	public float x() {
		return _x;
	}

	public float y() {
		return _y;
	}

	public String type() {
		return _type;
	}

	public String visitorId() {
		return _visitorId;
	}

	public ParkMovementLine setIndex(int index) {
		_index = index;
		return this;
	}

	public ParkMovementLine setTimestamp(DateTime timestamp) {
		_timestamp = timestamp;
		return this;
	}

	public ParkMovementLine setX(float x) {
		_x = x;
		return this;
	}

	public ParkMovementLine setY(float y) {
		_y = y;
		return this;
	}

	public ParkMovementLine setType(String type) {
		_type = type;
		return this;
	}

	public ParkMovementLine setVisitorId(String visitorId) {
		_visitorId = visitorId;
		return this;
	}

	@Override
	public String toString() {
		String s = "";
		s += "visitorId: " + visitorId() + "\n";
		s += "pos: " + x() + ", " + y() + "\n";
		s += "timestamp: " + timestamp() + "\n";
		return s;
	}
}
