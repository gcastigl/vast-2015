package itba.infoviz.vast.dinoworld.util;

import java.util.Scanner;

public class PeekableScanner {

	private Scanner _scanner;
	private String _peeked;

	public PeekableScanner(Scanner scanner) {
		_scanner = scanner;
	}

	public Scanner scanner() {
		return _scanner;
	}

	public boolean hasNextLine() {
		return _peeked != null && _scanner.hasNextLine();
	}

	public void consume() {
		_peeked = null;
	}

	public String nextLine() {
		if (_peeked != null) {
			String s = _peeked;
			_peeked = null;
			return s;
		}
		return _scanner.nextLine();
	}

	public String peekLine() {
		if (_peeked == null && _scanner.hasNext()) {
			_peeked = _scanner.nextLine();
		}
		return _peeked;
	}
}
