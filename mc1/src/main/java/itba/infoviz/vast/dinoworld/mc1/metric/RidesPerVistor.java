package itba.infoviz.vast.dinoworld.mc1.metric;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;

public class RidesPerVistor implements Runnable {

	public static void main(String[] args) {
		new RidesPerVistor(new File("./data/MC1 2015 Data/park-movement-Fri.csv")).run();
	}

	private File _file;

	public RidesPerVistor(File file) {
		_file = file;
	}

	@Override
	public void run() {
		Map<String, Integer> ridesPerVistor = new HashMap<>();
		try {
			Scanner scanner = new Scanner(_file);
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				if (line.contains("check-in")) {
					String[] columns = line.split(",");
					String visitorId = columns[1];
					Integer count = ridesPerVistor.get(visitorId);
					ridesPerVistor.put(visitorId, (count == null ? 0 : count) + 1);
				}
			}
			clusterResults(ridesPerVistor);
			scanner.close();
		} catch (FileNotFoundException e) {
			throw new IllegalStateException(e);
		}
	}

	private void clusterResults(Map<String, Integer> ridesPerVistor) {
		Map<Integer, Integer> vistorCountPerRidesCount = new HashMap<>();
		for (int ridesCount : ridesPerVistor.values()) {
			Integer count = vistorCountPerRidesCount.get(ridesCount);
			vistorCountPerRidesCount.put(ridesCount, (count == null ? 0 : count) + 1);
		}
		Collection<Integer> xs = new LinkedList<>();
		Collection<Integer> ys = new LinkedList<>();
		for (Entry<Integer, Integer> e : vistorCountPerRidesCount.entrySet()) {
			xs.add(e.getKey());
			ys.add(e.getValue());
		}
		System.out.println("xs = " + xs);
		System.out.println("ys = " + ys);
	}
}
